(function ($) {
  $(document).ready(function(){
	$('.navbar').fadeIn(900);    
});
  }(jQuery));
  
  $(document).keyup(function(event) {
      if ($("#searchbox1").is(":focus") && (event.keyCode == 13)) {
          $("#ab1").click();
      }
      if ($("#searchbox2").is(":focus") && (event.keyCode == 13)) {
          $("#ab2").click();
      }
  });
 
/* Anything that gets to the document
   will hide the dropdown */
$(document).on('click', function (e) {
    if (e.target.id != "searchbox2") {
        $(".navbar-collapse").collapse('hide');
    }
});
